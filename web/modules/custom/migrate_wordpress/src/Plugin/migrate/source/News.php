<?php

/**
 * @file
 * Contains \Drupal\migrate_wordpress\Plugin\migrate\source\News.
 */

namespace Drupal\migrate_wordpress\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * @MigrateSource(
 *   id = "wp_news"
 * )
 */
class News extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Get all the news, post_type=post filters out revisions and pages.
    $query = $this->select($this->configuration['table_prefix'] . 'posts', 'p')
      ->fields('p', array_keys($this->postFields()))
      ->condition('post_type', 'post')
      ->condition('post_status', ['publish', 'private', 'draft'], 'IN')
      ->orderBy('post_date', 'ASC');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $row->setSourceProperty('post_date', strtotime($row->getSourceProperty('post_date')));
    $row->setSourceProperty('post_modified', strtotime($row->getSourceProperty('post_modified')));
    $body = $row->getSourceProperty('post_content');

    $doc = new \DomDocument();
    \Drupal::logger('migrate_wordpress')->notice('Getting body: @body', array('@body' => $body));
    if (!empty($body)) {
      $load_options = array(LIBXML_HTML_NOIMPLIED, LIBXML_HTML_NODEFDTD);
      $body = mb_convert_encoding($body, 'HTML-ENTITIES', 'UTF-8');
///      @$doc->loadHTML($body, LIBXML_HTML_NOIMPLIED);
      @$doc->loadHTML($body);
      $doc->preserveWhiteSpace = false;
      # loadHTML causes a !DOCTYPE tag to be added, so remove it:
      $doc->removeChild($doc->firstChild);
      $images = $doc->getElementsByTagName('img');
      $links = $doc->getElementsByTagName('a');
      $image_sources = array();
      $link_hrefs = array();
      $file_hrefs = array();

      foreach($images as $im) {
//        \Drupal::logger('migrate_wordpress')->notice('Found image: @im in content', array('@im' => dpr($im)));
        $src = $im->getAttribute('src');
        $alt = $im->getAttribute('alt');
        $image_sources[] = array('src' => $src, 'alt' => $alt);
        $im->parentNode->removeChild($im);
      }

      // Now copy each over and add to node
      $imgs = array();
      foreach ($image_sources as $img) {
//        \Drupal::logger('migrate_wordpress')->notice('Attempting to add img: @img to field_image', array('@img' => $img));
        if ($data = file_get_contents(utf8_decode($img['src']))) {
          $img_parts = explode('/', $img['src']);
          $img_name = end($img_parts);
          $file = file_save_data($data, 'public://' . $img_name, FILE_EXISTS_RENAME);
          $imgs[] = array('target_id' => $file->id(), 'alt' => $img['alt']);
        }
      }
      if (count($imgs) > 0) {
        $row->setSourceProperty('field_image', $imgs);
      } else { // Check for wp thumnail post
        $sourceid = $row->getSourceProperty('id');
        $imgs = array();
        $result = db_query("SELECT p.post_name, guid FROM wp_zcn.wp_postmeta pm INNER JOIN wp_zcn.wp_posts p ON pm.meta_value = p.ID WHERE pm.post_id = :post_id AND pm.meta_key = :key_type", array(':post_id' => $sourceid, ':key_type' => '_thumbnail_id'));
        foreach ($result as $record) {
          \Drupal::logger('migrate_wordpress')->notice('Grabbing image thumbnail for id: @id, url: @url', array('@id' => $sourceid, '@url' => $record->guid));
          if ($data = file_get_contents(utf8_decode($record->guid))) {
            $img_parts = explode('/', $record->guid);
            $img_name = end($img_parts);
            $file = file_save_data($data, 'public://' . $img_name, FILE_EXISTS_RENAME);
            $imgs[] = array('target_id' => $file->id(), 'alt' => $img['alt']);
          }
          if (count($imgs) > 0) {
            $row->setSourceProperty('field_image', $imgs);
          }
        }
      }

      $hrefs = array();
      foreach ($links as $link) {
        // Strip out any empty a tags
        if (!$link->hasChildNodes()) {
          $link->parentNode->removeChild($link);
        }
      }

      // Find and associate an author profile
      $author_id = $row->getSourceProperty('post_author');
      $author_name = db_query("SELECT display_name FROM wp_zcn.wp_users WHERE ID = :author_id", array(':author_id' => $author_id))->fetchField();
      // Strip the name down to all before first comma
      $author_name = substr($name, 0, strpos($name, ','));
      $author_nid = db_query("SELECT nid FROM node_field_data WHERE type = :type AND title LIKE :name", array(':type' => 'author_profile', ':name' => "%$author_name%"))->fetchField();
      if ($author_nid > 0) {
        $author[] = array('target_id' => $author_nid);
        $row->setSourceProperty('field_news_author', $author);
      }

      $body = $doc->saveHTML();
      // Strip out opening and closing body & html tags
      $replace = array('<body>', '</body>', '<html>', '</html>');
      $body = str_replace($replace, '', $body);
//      $row->setSourceProperty('post_content', preg_replace('<img(.*?)/>is', '', $body));
      // Convert double returns to <p> tags
      $searchFor = array("\n\n", "\n\r");
      $body = "<p>" . str_replace($searchFor, "</p>\n<p>", $body);
      if (substr($body, -4, 4) != "<\p>") {
        $body .= '<\p>';
      }

      $row->setSourceProperty('post_content', $body);
    }
    return parent::prepareRow($row);
  }

  /**
   * Returns the Posts fields to be migrated.
   *
   * @return array
   *   Associative array having field name as key and description as value.
   */
  public function postFields() {
    $fields = array(
      'id' => $this->t('The Post ID'),
      'post_author' => $this->t('The post author.'),
      'post_date' => $this->t('The date the post was created.'),
      'post_content' => $this->t('The post content'),
      'post_title' => $this->t('The title.'),
      'post_excerpt' => $this->t('The post excerpt'),
      'post_status' => $this->t('The status of the post.'),
      'post_name' => $this->t('The machine name of the post.'),
      'post_modified' => $this->t('The last modified time.'),
      'post_type' => $this->t('The post type.'),
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->postFields();
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function bundleMigrationRequired() {
    return FALSE;
  }
  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return array(
      'id' => array(
        'type' => 'integer',
        'alias' => 'p',
      ),
    );
//    $ids['ID']['type'] = 'integer';
//    $ids['ID']['alias'] = 'p';
//    return $ids;
  }

}
