<?php
/**
 * @file
 * Contains \Drupal\migrate_wordpress\Plugin\migrate\source\Users.
 */

namespace Drupal\migrate_wordpress\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Extract users from Wordpress database.
 *
 * @MigrateSource(
 *   id = "wp_users"
 * )
 */
class Users extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $allowed_names = array('kmartin', 'mshaivitz', 'lbasall', 'Shannon Davis', 'ltully', 'efutrell', 'alee', 'hfinn', 'sara.mazursky', 'akott1', 'ddickson', 'aldyc', 'bcarr', 'donald.wertlieb', 'edwinbckmagloire', 'eperez', 'eursula', 'jaroslava_lemus', 'jlainez', 'kimstarmartin', 'lodonnell', 'loreto.barcelo', 'lsanchez', 'mkm', 'Nerina.SANCHEZ', 'rachel', 'ramon_soto', 'robert', 'SCZEA', 'shannon.river', 'swilkinson', 'jhermida');
//select * from wp_users u inner join wp_usermeta um on u.ID = um.user_id where um.meta_key = 'wp_capabilities';
    $query = $this->select('wp_users', 'u')
      ->fields('u', array_keys($this->userFields()))
      ->condition('u.user_login', $allowed_names, 'IN');
    $query->join('wp_usermeta', 'um', 'u.ID = um.user_id');
    $query->fields('um', array('meta_value'));
    $query->condition('um.meta_key', 'wp_capabilities');
    return $query;
  }

  /**
   * Returns the User fields to be migrated.
   *
   * @return array
   *   Associative array having field name as key and description as value.
   */
  protected function userFields() {
    $fields = array(
      'id' => $this->t('User ID'),
      'user_login' => $this->t('Username'),
      'user_pass' => $this->t('Password'),
      'user_email' => $this->t('Email address'),
      'user_registered' => $this->t('Created time'),
    );
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->userFields();
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function bundleMigrationRequired() {
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeId() {
    return 'users';
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return array(
      'id' => array(
        'type' => 'integer',
        'alias' => 'u',
      ),
    );
  }

}
